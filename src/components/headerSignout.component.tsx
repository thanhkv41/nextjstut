"use client"
import { signIn, signOut } from "next-auth/react";
import {
    Group,
    Button,
    Box,
  } from '@mantine/core';
  import { MantineLogo } from '@mantine/ds';
  import classes from './HeaderMegaMenu.module.css';
  
  
  export function HeaderMegaMenuSignOut() {
  
    return (
      <div style={{width: "100%"}}>
        <Box pb={120}>
        <header className={classes.header}>
          <Group justify="space-between" h="100%">
            <MantineLogo size={30} />
  
            <Group visibleFrom="sm">
              <a href="/auth/signin"><Button onClick={() => signOut()}>Sign out</Button></a>
              <a href="/profile"><Button>Profile</Button></a>
            </Group>
          </Group>
        </header>
      </Box>
      </div>
    );
  }