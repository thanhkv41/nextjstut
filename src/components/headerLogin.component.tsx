"use client"
import {
    Group,
    Button,
    Box,
  } from '@mantine/core';
  import { MantineLogo } from '@mantine/ds';
  import classes from './HeaderMegaMenu.module.css';
  
  
  export function HeaderMegaMenuLogin() {
  
    return (
        <div style={{width:"100%"}}>
        <header className={classes.header}>
          <Group justify="space-between" w="100%" h="100%">
            <a href="/"><MantineLogo size={30} /></a>
  
            <Group visibleFrom="sm">
              <a href="/auth/signin"><Button>Log in</Button></a>
              <a href="/register"><Button>Sign up</Button></a>
              <a href="/profile"><Button>Profile</Button></a>
            </Group>
          </Group>
        </header>
      </div>
    );
  }