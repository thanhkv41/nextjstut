import { RegisterForm } from "./form";
export default function RegisterPage() {
  return (
    <div
    style={{
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      flexWrap: "wrap"
    }}
    >
      <div>
        <RegisterForm />
      </div>
    </div>
  );
}