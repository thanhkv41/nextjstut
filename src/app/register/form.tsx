"use client";

import { signIn } from "next-auth/react";
import { ChangeEvent, useState } from "react";
import { TextInput, rem } from "@mantine/core";
import {
  Box,
  Progress,
  PasswordInput,
  Group,
  Text,
  Center,
} from "@mantine/core";
import { IconCheck, IconX } from "@tabler/icons-react";
function PasswordRequirement({
  meets,
  label,
}: {
  meets: boolean;
  label: string;
}) {
  return (
    <Text component="div" c={meets ? "teal" : "red"} mt={5} size="sm">
      <Center inline>
        {meets ? (
          <IconCheck size="0.9rem" stroke={1.5} />
        ) : (
          <IconX size="0.9rem" stroke={1.5} />
        )}
        <Box ml={7}>{label}</Box>
      </Center>
    </Text>
  );
}

const requirements = [
  { re: /[0-9]/, label: "Includes number" },
  { re: /[a-z]/, label: "Includes lowercase letter" },
  { re: /[A-Z]/, label: "Includes uppercase letter" },
  { re: /[$&+,:;=?@#|'<>.^*()%!-]/, label: "Includes special symbol" },
];

function getStrength(password: string) {
  let multiplier = password.length > 5 ? 0 : 1;

  requirements.forEach((requirement) => {
    if (!requirement.re.test(password)) {
      multiplier += 1;
    }
  });

  return Math.max(100 - (100 / (requirements.length + 1)) * multiplier, 0);
}

export const RegisterForm = () => {
  let [loading, setLoading] = useState(false);
  let [formValues, setFormValues] = useState({
    name: "",
    email: "",
    password: "",
    repeatPassword: "",
  });
  const onSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    const { name, email, password } = formValues;
    const formData = {
      name,
      email,
      password,
    };
    if (formValues.password != formValues.repeatPassword) {
      alert("Your password doesn't match");
      return;
    }
    setLoading(true);
    try {
      const res = await fetch("/api/register", {
        method: "POST",
        body: JSON.stringify(formData),
        headers: {
          "Content-Type": "application/json",
        },
      });

      setLoading(false);
      if (!res.ok) {
        alert("Đăng ký thất bại!");
        return;
      }

      signIn(undefined, { callbackUrl: "/" });
    } catch (error: any) {
      setLoading(false);
      console.error(error);
      alert(error.message);
    }
  };

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setFormValues({ ...formValues, [name]: value });
  };
  const checks = requirements.map((requirement, index) => (
    <PasswordRequirement
      key={index}
      label={requirement.label}
      meets={requirement.re.test(formValues.password)}
    />
  ));
  //Check strength password
  const strength = getStrength(formValues.password);
  const bars = Array(4)
    .fill(0)
    .map((_, index) => (
      <Progress
        styles={{ section: { transitionDuration: "0ms" } }}
        value={
          formValues.password.length > 0 && index === 0
            ? 100
            : strength >= ((index + 1) / 4) * 100
            ? 100
            : 0
        }
        color={strength > 80 ? "teal" : strength > 50 ? "yellow" : "red"}
        key={index}
        size={4}
      />
    ));
  return (
    <form
      onSubmit={onSubmit}
      style={{
        display: "flex",
        flexDirection: "column",
        width: 500,
        rowGap: 10,
      }}
    >
      <h1 style={{textAlign:"center"}}>Register</h1>
      <a style={{textAlign: "center"}} href="/auth/signin">Have an account? Login here !</a>
      <TextInput
        required
        name="name"
        value={formValues.name}
        onChange={handleChange}
        label="Name"
        placeholder="Your name"
      />
      <TextInput
        required
        type="text"
        name="email"
        value={formValues.email}
        onChange={handleChange}
        label="Email"
      />
      <div>
        <PasswordInput
          value={formValues.password}
          name="password"
          onChange={handleChange}
          placeholder="Your password"
          label="Password"
          required
        />

        <Group gap={5} grow mt="xs" mb="md">
          {bars}
        </Group>

        <PasswordRequirement
          label="Has at least 6 characters"
          meets={formValues.password.length > 5}
        />
        {checks}
      </div>
      <PasswordInput
        value={formValues.repeatPassword}
        name="repeatPassword"
        onChange={handleChange}
        placeholder="Repet your password"
        label="Repeat your password"
        required
      />
      <PasswordRequirement
        label="Match your password! "
        meets={
          formValues.password == formValues.repeatPassword &&
          formValues.password != ""
        }
      />
      <button
        style={{
          backgroundColor: `${loading ? "#ccc" : "#3446eb"}`,
          color: "#fff",
          padding: "1rem",
          cursor: "pointer",
        }}
        disabled={loading}
      >
        {loading ? "loading..." : "Register"}
      </button>
    </form>
  );
};
