"use client";
import {
  TextInput,
  PasswordInput,
  Checkbox,
  Anchor,
  Paper,
  Title,
  Text,
  Container,
  Group,
  Button,
} from "@mantine/core";
import classes from "./AuthenticationTitle.module.css";
import { signIn } from "next-auth/react";
import { useState } from "react";

const AuthenticationTitle = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const onSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    if (!email) alert("Vui lòng nhập gmail!");
    else if (!password) alert("Vui lòng nhập password!");
    else {
      let result: any | undefined = await signIn("credentials", {
        email,
        password,
        redirect: false,
      });
      if (result.error) {
        // Xử lý lỗi đăng nhập (ví dụ: hiển thị thông báo lỗi)
        alert("Đăng nhập thất bại!");
      } else if (result?.status === 200) {
        // Đăng nhập thành công
        window.location.href = "/";
      }
    }
  };

  return (
    <Container size={420} my={40}>
      <Title ta="center" className={classes.title}>
        Welcome back!
      </Title>
      <Text c="dimmed" size="sm" ta="center" mt={5}>
        Do not have an account yet?{" "}
        <a href="/register">
          <Anchor size="sm" component="button">
            Create account
          </Anchor>
        </a>
      </Text>
      <form>
        <Paper withBorder shadow="md" p={30} mt={30} radius="md">
          <TextInput
            onChange={(e) => {
              setEmail(e.target.value);
            }}
            label="Email"
            placeholder="you@mantine.dev"
            required
          />
          <PasswordInput
            onChange={(e) => {
              setPassword(e.target.value);
            }}
            label="Password"
            placeholder="Your password"
            required
            mt="md"
          />
          <Group justify="space-between" mt="lg">
            <Checkbox label="Remember me" />
            <Anchor component="button" size="sm">
              Forgot password?
            </Anchor>
          </Group>
          <Button onClick={(e) => onSubmit(e)} fullWidth mt="xl">
            Sign in
          </Button>
        </Paper>
      </form>
    </Container>
  );
};
export default AuthenticationTitle;
