import { HeaderMegaMenuSignOut } from "@/components/headerSignout.component";
import { HeaderMegaMenuLogin } from "@/components/headerLogin.component";
import { authOptions } from "@/lib/auth";
import { getServerSession } from "next-auth";
import ListUsers from "../ListUsers";
type User = {
    id: number;
    name: string;
    email: string;
  };
  
  export default async function Profile() {
    const users: User[] = await fetch(
      "https://jsonplaceholder.typicode.com/users"
    ).then((res) => res.json());
    const session = await getServerSession(authOptions);
    return (
      <main style={{ maxWidth: 1200, marginInline: "auto", padding: 20 }}>
          {session ? <HeaderMegaMenuSignOut /> : <HeaderMegaMenuLogin />}
        <ListUsers />
      </main>
    );
  }