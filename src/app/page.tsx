import { getServerSession } from "next-auth";
import { authOptions } from "@/lib/auth";
import { User } from "@/components/user.component";
import { HeaderMegaMenuSignOut } from "@/components/headerSignout.component";
import { HeaderMegaMenuLogin } from "@/components/headerLogin.component";
export default async function Home() {
  const session = await getServerSession(authOptions);
  console.log(session);

  return (
    <main
      style={{
        // display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "70vh",
        width: "100%",
      }}
    >
      <div>
        {session ? <HeaderMegaMenuSignOut /> : <HeaderMegaMenuLogin />}

        <div style={{ textAlign: "center" }}>
          <h1>Server Session</h1>
          <pre>{JSON.stringify(session)}</pre>
          <User />
        </div>
      </div>
    </main>
  );
}
